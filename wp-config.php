<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'impact-events_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nix^[Kt&48JwMPx*o]{)$|Xh#xW~?Y&k|8m7o6?2X9kk_QjTdGj{9mAk,5m-pDPV');
define('SECURE_AUTH_KEY',  ']Gn=W]nl_9<Pu^nte0U8`_v&AWhs7`)+aV2`7m^ccO/Ef0!tE+yfNt&N[8xf;<pi');
define('LOGGED_IN_KEY',    'k>}qXk=Njwj`atIbu`Em#0U<4ULT?F9^ZVWu,+^%ofp^_r4 YhRRL b-p&j52NAA');
define('NONCE_KEY',        '/ b7eUg|dhN4]MKGEmB7ZpC?IW&C5U0-J=^Jx;(#|8-Pd ~qcLzFduJ8{+[+u;$h');
define('AUTH_SALT',        'c8v%jEj`-&]]9>heX<n|[!-tsnSRZ$7{Q;JCxe7`y/},D,n:oG0q$B0%(7^#Lq>9');
define('SECURE_AUTH_SALT', '%6~I4@vcy|TMS?BE*4t9=AB3QfPT]r&4b2,-S7YK>-RXWBwlMIMJ++67(+8ZD|x6');
define('LOGGED_IN_SALT',   '0r5(R)i3):t>+RatBm..O.S?MQ0|k{e1%[hgTX12nF5e^x9`%-}[zoyh/`|$.jVP');
define('NONCE_SALT',       'e0E7vb|0+*.j(2m]Cw5-yYZ70:ISYd%fC~M3@g]nfpDWI((|xf|#cz/B@g-!iddo');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
