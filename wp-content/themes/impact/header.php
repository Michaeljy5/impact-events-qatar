<!DOCTYPE html>
<html>
	<head>
		<?php wp_enqueue_script("jquery"); ?>
		<link rel="stylesheet" href="<?php bloginfo('template_url');?>/style.css"/>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale: 1.0">
		<title>
			<?php wp_title('|', 'true', 'right');bloginfo('name'); ?>
		</title>	
		<!-- Latest compiled and minified CSS -->
		<link rel="icon" type="image/png" href="<?php bloginfo('template_url');?>/images/logo.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link href="<?php bloginfo('template_url');?>/bxslider/jquery.bxslider.css" rel="stylesheet" />
		<?php wp_head(); ?>
	</head>
<body id="home">

<div class="toggle-nav" id="toggle-nav">
	<div class="back-btn">
			<i class="fa fa-angle-right" id="toggle-btn"></i>
	</div>
	<div class="toggle-nav-content">
		<ul class="toggle-navigation">
			  <li><a href="http://localhost/impact-events/">HOME</a></li>
			  <li id="services"><a href="services">SERVICES</a></li>
			  <li id="experience"><a href="experience">EXPERIENCE</a></li>
			  <li id="team"><a href="team">TEAM</a></li>
			  <li id="contact"><a href="contact-us">CONTACT US</a></li>
		</ul>
	</div>
</div>

	<div class="page-content">
		<header class="header-content"> 
			<div class="header-container">
				<div class="nav-logo">
					<a href="home"><img height=70 width=120 src="<?php bloginfo('template_url');?>/images/logo.png"></a>
				</div>
				<div class="header-nav">
					<div class="main-nav">
					<ul id="navigation">
					  <li><a href="http://localhost/impact-events/">HOME</a></li>
					  <li id="services"><a href="services">SERVICES</a></li>
					  <li id="experience"><a href="experience">EXPERIENCE</a></li>
					  <li id="team"><a href="team">TEAM</a></li>
					  <li id="contact"><a href="contact-us">CONTACT US</a></li>
					</ul>
				</div>
			</div>
			<div class="toggle-btn">
				<i class="fa fa-bars" id="toggle-btn"></i>
			</div>		 
		</header>



