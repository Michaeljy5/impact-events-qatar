<?php 
// Enable Theme Supports
if (function_exists ('add_theme_support')){ 
add_theme_support('menus');
add_theme_support('post-thumbnails');
}

//Enable image resize/crop
if (function_exists ('add_image_size')){ 
add_image_size( 'featured', 300, 200, false); //Latest posts thumb
add_image_size( 'post-thumb', 150, 400, false); //Bottom featured thumb
}

//Register Menus
if (function_exists ('register_nav_menus')){ 
register_nav_menus(array(	
'main-menu' => __('Primary Menu'),
'footer-menu' => __('Footer Menu')
));
}

if (function_exists ('register_sidebar')){ 
	
register_sidebar(array(
'name'          => 'contact-form',
'id'            => 'contact-form',
'before_widget' => '<li id="%1$s" class="widget %2$s">',
'after_widget'  => '</li>',
'before_title'  => '<h2 class="widgettitle">',
'after_title'   => '</h2>'
));

register_sidebar(array(
'name'          => 'social-share',
'id'            => 'social-share',
'before_widget' => '<li id="%1$s" class="widget %2$s">',
'after_widget'  => '</li>',
'before_title'  => '<h2 class="widgettitle">',
'after_title'   => '</h2>'
));

}

// Numbered Pagination
if ( !function_exists( 'wpex_pagination' ) ) {
	
	function wpex_pagination() {
		
		$prev_arrow = is_rtl() ? '&raquo;' : '&laquo;';
		$next_arrow = is_rtl() ? '&laquo;' : '&raquo;';
		
		global $wp_query;
		$total = $wp_query->max_num_pages;
		$big = 999999999; // need an unlikely integer
		if( $total > 1 )  {
			 if( !$current_page = get_query_var('paged') )
				 $current_page = 1;
			 if( get_option('permalink_structure') ) {
				 $format = 'page/%#%/';
			 } else {
				 $format = '&paged=%#%';
			 }
			echo paginate_links(array(
				'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'		=> $format,
				'current'		=> max( 1, get_query_var('paged') ),
				'total' 		=> $total,
				'mid_size'		=> 2,
				'type' 			=> 'list',
				'prev_text'		=> $prev_arrow,
				'next_text'		=> $next_arrow,
			 ) );
		}
	}	
}

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'custom_trim_excerpt');

function custom_trim_excerpt($text) { // Fakes an excerpt if needed
global $post;
if ( '' == $text ) {
$text = get_the_content('');
$text = apply_filters('the_content', $text);
$text = str_replace(']]>', ']]>', $text);
$text = strip_tags($text);
$excerpt_length = 40;
$words = explode(' ', $text, $excerpt_length + 1);
if (count($words) > $excerpt_length) {
array_pop($words);
array_push($words, '...');
$text = implode(' ', $words);
}
}
return $text;
}

function is_type_page() { // Check if the current post is a page
	global $post;

	if ($post->post_type == 'page') {
		return true;
	} else {
		return false;
	}
}

function wp_pagenavi($before = '', $after = '', $prelabel = '', $nxtlabel = '', $pages_to_show = 5, $always_show = false, $type = "") {
    global $request, $posts_per_page, $wpdb, $paged,$wp_query;
	if($type == "page")
	{
		$request = $wp_query->request;
			//$request = "SELECT SQL_CALC_FOUND_ROWS wp_posts.ID FROM wp_posts WHERE 1=1 AND wp_posts.post_type = 'post' AND (wp_posts.post_status = 'publish') ORDER BY wp_posts.post_date DESC LIMIT 0, 10";
	}

    if (empty($prelabel)) {
        $prelabel = '<label class="pages-link">&lsaquo;</label>';
    } if (empty($nxtlabel)) {
        $nxtlabel = '<label class="pages-link">&rsaquo;</label>';
    } $half_pages_to_show = round($pages_to_show / 2);
    if (!is_single()) {
        if (!is_category()) {
            preg_match('#FROM\s(.*)\sORDER BY#siU', $request, $matches);
        } else {
            preg_match('#FROM\s(.*)\sGROUP BY#siU', $request, $matches);
        }
        $fromwhere = $matches[1];
        $numposts = $wpdb->get_var("SELECT COUNT(DISTINCT ID) FROM $fromwhere");
        $max_page = ceil($numposts / $posts_per_page);
        if (empty($paged)) {
            $paged = 1;
        }
        if ($max_page > 1 || $always_show) {
            echo "$before <div class='pagination-content-new'>";
            if ($paged >= ($pages_to_show - 1)) {
                echo '<a class="pages-link-nav" href="' . get_pagenum_link() . '">&laquo; </a> ';
            }
            previous_posts_link($prelabel);
            for ($i = $paged - $half_pages_to_show; $i <= $paged + $half_pages_to_show; $i++) {
                if ($i >= 1 && $i <= $max_page) {
                    if ($i == $paged) {
                        echo "<strong class='on'>$i</strong>";
                    } else {
                        echo ' <a class="pages-link" href="' . get_pagenum_link($i) . '">' . $i . '</a> ';
                    }
                }
            }
            next_posts_link($nxtlabel, $max_page);
            if (($paged + $half_pages_to_show) < ($max_page)) {
                echo ' <a class="pages-link-nav" href="' . get_pagenum_link($max_page) . '"> &raquo;</a>';
            }
            echo "</div> $after";
        }
    }
}

function get_nav_url($category)
{
	$home_url =  get_home_url(); 
	$page_id = "";
	
	if(strtoupper ($category) == "TRAVEL")
	{
		$page_id ="?page_id=9";
	}
	else if(strtoupper ($category) == "FOOD")
	{
		$page_id ="?page_id=8";
	}	
	else if(strtoupper ($category) == "BEAUTY")
	{
		$page_id ="?page_id=69";
	}	
	else if(strtoupper ($category) == "FASHION")
	{
		$page_id ="?page_id=6";
	}		
	else if(strtoupper ($category) == "FUN STUFF")
	{
		$page_id ="?page_id=70";
	}		
	else if(strtoupper ($category) == "ME TIME")
	{
		$page_id ="?page_id=72";
	}		
	else if(strtoupper ($category) == "CONTACT ME")
	{
		$page_id ="?page_id=109";
	}		
	return $home_url."/".$page_id;
}
?>



 