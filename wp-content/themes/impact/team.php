<?php
/*
 Template Name: team
*/
?>
<link rel="icon" type="image/png" href="<?php bloginfo('template_url');?>/images/logo.png">
<body id="team">
<?php get_header(); ?>

<div class="main-content">
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/hero_home.jpg') no-repeat; background-size:cover;">
		<div class="cover-label-home">
			<h1 class="label-title">The experts in events</h1>
			<h3 class="label-desc">We work closely with our clients to deliver events that exceed all expectations.</h3>
			<div class="clear"></div>
		</div>
	</div>

	<!-- Team Members -->
	<div style="height: 40px;"></div>

	<div class="team-content" id="Russell-John-Mason">
		<div class="team-img"><img src="<?php bloginfo('template_url');?>/images/team_russell.jpg"></div>
		<div class="team-desc">
			<div class="team-name"><h3>Russell John Mason</h3></div>
			<div class="team-title"><h4>Managing Partner</h4>
				<div style="height:25px;"></div>
				<hr style="display:block;width:3%;position:absolute;float:left;margin-top:0;border-color:#691A40;">
			</div>
			<div class="team-info"><p>Russell Mason has been in the events industry for 35 years, in over 40 different countries - including all GCC.
			                       He has been involved in different aspects of events including live world-class musical productions, international
			                       exhibitions and international sports events amongst others. Mr. Mason has worked on the Qatar National Day events in 
			                       four different years, involving event consultancy, fireworks consultancy and logistics.</p></div>
		</div>
	</div>

		<div class="team-content" id="Perry-Malcolm-Smith">
		<div class="team-img"><img src="<?php bloginfo('template_url');?>/images/team_perry.jpg"></div>
		<div class="team-desc">
			<div class="team-name"><h3>Perry Malcolm Smith</h3></div>
			<div class="team-title"><h4>Chief Executive Officer</h4>
				<div style="height:25px;"></div>
				<hr style="display:block;width:3%;position:absolute;float:left;margin-top:0;border-color:#691A40;">
			</div>
			<div class="team-info"><p>Perry Smith has 20 years experience managing and directing unique VVIP, Corporate and Sports events in Oman, UAE and
									  Qatar, during which he has operated on very close personal basis with Royalty, Ministers and global VVIPs.</p></div>
		</div>
	</div>

	<div class="team-content" id="Ann-Karolyn-Cunano">
		<div class="team-img"><img src="<?php bloginfo('template_url');?>/images/team_ann.jpg"></div>
		<div class="team-desc">
			<div class="team-name"><h3>Ann Karolyn Cunano</h3></div>
			<div class="team-title"><h4>Project Manager and Quality Control</h4>
				<div style="height:25px;"></div>
				<hr style="display:block;width:3%;position:absolute;float:left;margin-top:0;border-color:#691A40;">
			</div>
			<div class="team-info"><p>With a marketing and customer service background, Ann Cunano brings an important 
									  role in ensuring quality standards within the projects of Impact Events. She has been
									  working in the events industry in Qatar for the past 6 years.</p></div>
		</div>
	</div>

	<div class="team-content" id="Paul-Sayer">
		<div class="team-img"><img src="<?php bloginfo('template_url');?>/images/team_paul.jpg"></div>
		<div class="team-desc">
			<div class="team-name"><h3>Paul Sayer</h3></div>
			<div class="team-title"><h4>Project Manager and Master Carpenter</h4>
				<div style="height:25px;"></div>
				<hr style="display:block;width:3%;position:absolute;float:left;margin-top:0;border-color:#691A40;">
			</div>
			<div class="team-info"><p>Paul Sayer has worked on numerous high profile events worldwide and in key territories such as africa, GCC,
			                          Southeast Asia and Europe. His master craft in carpentry to produce bespoke and finely finished outcome combined
			                          with his project managing skills, make him a valuable member of Impact Events.</p></div>
		</div>
	</div>

	<div class="team-content" id="Saroj-Choudhary">
		<div class="team-img"><img src="<?php bloginfo('template_url');?>/images/team_saroj.jpg"></div>
		<div class="team-desc">
			<div class="team-name"><h3>Saroj Choudhary</h3></div>
			<div class="team-title"><h4>Crew Chief</h4>
				<div style="height:25px;"></div>
				<hr style="display:block;width:3%;position:absolute;float:left;margin-top:0;border-color:#691A40;">
			</div>
			<div class="team-info"><p>"Choudhary" as he likes to be called, is another integral part of the Impact Team as his skills
								       and resourcefulness become invaluable during an event. His leadership skills and knowledge make it easy for
								       him to organize and provide logistical suppport services such as trucking, labor, crews, carpenters, carpet laying
								       and metal workers for any project.</p></div>
		</div>
	</div>

	<div class="team-content" id="Faisal-Paduvingal">
		<div class="team-img"><img src="<?php bloginfo('template_url');?>/images/team_faisal.jpg"></div>
		<div class="team-desc">
			<div class="team-name"><h3>Faisal Paduvingal</h3></div>
			<div class="team-title"><h4>Production Runner</h4>
				<div style="height:25px;"></div>
				<hr style="display:block;width:3%;position:absolute;float:left;margin-top:0;border-color:#691A40;">
			</div>
			<div class="team-info"><p>Faisal Paduvingal is Impact Events' company production runner, who has an integral knowledge
			                          of Doha. His resourcefulness and ability to communicate with local suppliers ensures that he 
			                          delivers what is expected of him, and more.</p></div>
		</div>
	</div>
	<!-- get in touch -->
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/bg_services in home.jpg') no-repeat;background-size:cover; height: 500px;">
		<div class="hero-img-quote">
			<p>"We strongly believe that good ideas and dynamic presentation do not necessarily lead to a large expense.
			 We always look to deliver "value for money" events, maximizing the budget available to deliver the best
			 possible presentation."</p>
			 <p class="quote-author">Ann Cunano, Commercial Bank Qatar</p>
		</div>
	</div>

	<div class="event-help">
		<h3>Need help with your event?</h3>
		<a class="button" href="contact-us">GET IN TOUCH &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a></p>
	</div>
	
</div>

<?php get_footer(); ?> 