<?php
	$send_to="michaeljyambao@gmail.com";
	
	// info@impacteventsqatar.com

		if(!isset($_POST['your-name']) ||
	 
			!isset($_POST['your-email']) ||
	 
			!isset($_POST['your-message'])) {
	 
			echo 'We are sorry, but there appears to be a problem with the form you submitted.';    
			die(0);			
		}		
		$error_message ="";
		$name = $_POST['your-name'];
		$from_email = $_POST['your-email'];
		$message = $_POST['your-message'];
		
		$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
		if(!preg_match($email_exp,$from_email)) {
			echo 'The Email Address you entered does not appear to be valid.';
			die(0);		
		}			
		$string_exp = "/^[A-Za-z .'-]+$/";
		if(!preg_match($string_exp,$name)) {
			echo 'The First Name you entered does not appear to be valid.';
			die(0);		
		}	
		if(strlen($message) < 2) {
			echo 'The Comments you entered do not appear to be valid.';
			die(0);		
		}			
		if(strlen($error_message) > 0) {
			echo ('We are sorry, but there appears to be a problem with the form you submitted.');       
			die(0);		
		}		
		$from="From: $name<$from_email>\r\nReturn-path: $from_email"; 
		$message = $message . "\n\n
		--\n
		This e-mail was sent from IMPACT EVENTS QATAR (http://impacteventsqatar.com)";
		$success=@mail($send_to, $subject, $message, $from); 
		if($success!=1)
		{
			echo "Your message was not sent. Please email info@impacteventsqatar.com";
			die(0);		
		}			
		else
		{
			echo "Your message was sent.";
		}
			
?>

