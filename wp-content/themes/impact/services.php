<?php
/*
 Template Name: services
*/
?>
<link rel="icon" type="image/png" href="<?php bloginfo('template_url');?>/images/logo.png">
<body id="services">
<?php get_header(); ?>

<div class="main-content">
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/hero_home.jpg') no-repeat; background-size:cover;">
		<div class="cover-label-home">
			<h1 class="label-title">From planning to execution</h1>
			<h3 class="label-desc">We offer complete events solution, that creates experiences out of the ordinary.</h3>
			<div class="clear"></div>
		</div>
	</div>

	<!-- Services -->
	<div class="service-content" id="events-management-and-consultancy">
		<div class="service-logo"><img src="<?php bloginfo('template_url');?>/images/icon_1mgmtandconsult.png"></div>
		<div class="service-title"><h3>Events Management and Consultancy</h3></div>
		<div class="service-desc"><p>For the audience, a successful event shoud appear as a seamless spectacle, providing all that
		                             it promised and more. However, as we in the industry all know, a successful event is the result
		                             of countless hours of hard work bringing together diverse talents, the resolution of a seemingly
		                             endless series of obtacles and daily problems, and the overcoming of bureaucratic hurdles.</p></div>
        <div class="service-desc2"><p>This is where Impact Events Qatar excels, as we have the experience, contacts, and support 
        	                          services to make your event, opening, exhbition, or product launch a success.</p></div>

        <div class="service-desc3"><a href=""><p>What's it like to manage events in Qatar?<a/> &nbsp;<i class="fa fa-question-circle fa-lg" style="color:#691A40;"></i></p></div>
	</div> 

	<div class="service-content" id="logistical-support">
		<div class="service-logo"><img src="<?php bloginfo('template_url');?>/images/icon_2logistical.png"></div>
		<div class="service-title"><h3>Logistical Support</h3></div>
		<div class="service-desc"><p>We have a complete range of logistical support for your event. We have experience in sourcing and providing hard to find specialized equipment,
		                             materials, and crew at a fair price. We have proven experience in coordinating your transportation, catering, and accommodation needs - whatever their size
		                             or complexity. Perhaps most importantly, we have the flexibility to adapt to any last minute changes or requests you may have.</p></div>
	</div>

	<div class="service-content" id="exhibition-stands">
		<div class="service-logo"><img src="<?php bloginfo('template_url');?>/images/icon_3exhibition.png"></div>
		<div class="service-title"><h3>Exhibition Stands</h3></div>
		<div class="service-desc"><p>We are able to build custom exhibition stands to meet your particular requirements. We work closely with the international branding company icon to supply
		                             exhibit stands of the highest professional quality.</p></div>
	</div>


	<!-- See More Examples -->
	<div style="height:30px;"></div>
	<div class="see-more-content">
		<div class="see-more-img1" style="background: url('<?php bloginfo('template_url');?>/images/qiff_a.jpg') no-repeat; background-size:cover;">
			<a class="overlay" href="experience"><div class="see-more-btn">
				<h2>See an<br>Example</h2>
				<hr style="display:block;width:15%; text-align:center; margin-top:20px; border: 1px solid #fff; color:#fff;">
			</div></a>
		</div>
		<div class="see-more-img2" style="background: url('<?php bloginfo('template_url');?>/images/cbq_a.jpg') no-repeat; background-size:cover;"></div>
	</div>
	<div style="height:30px;"></div>


	<div class="service-content" id="stage_set_fit-out_prop_seating-construction">
		<div class="service-logo"><img src="<?php bloginfo('template_url');?>/images/icon_4stage.png"></div>
		<div class="service-title"><h3>Stage, Set, Fit-out, Prop, Seating Construction.</h3></div>
		<div class="service-desc"><p>Over the years, we have gained a strong reputation in being able to supply clients with professional grade custom built stage, set, prop, or seating to suit
		                             the particular needs of their event, exhibition, opening or conference.</p></div>
	</div>

	<div class="service-content" id="exhibitions_openings_product-launches">
		<div class="service-logo"><img src="<?php bloginfo('template_url');?>/images/icon_5openings etc.png"></div>
		<div class="service-title"><h3>Exhibitions, Openings, Product Launches</h3></div>
		<div class="service-desc"><p>We have the ability to fully support your needs for exhibitions, openings and product launches. We have expertise in all of these areas and are happy to tailor 
			                         our services to your specific needs.</p></div>
	</div>

	<div class="service-content" id="event-workforce-support">
		<div class="service-logo"><img src="<?php bloginfo('template_url');?>/images/icon_6event workforce.png"></div>
		<div class="service-title"><h3>Event Workforce Support</h3></div>
		<div class="service-desc"><p>Impact Events Qatar has the workforce to provide you with the support you need for your event. From the top down, we are a thoroughly professional company, attendant to all our clients need.
		                             We have already access to reliable local crews, carpenters and cleaners, and have many local licenced expert professionals such as riggers, lighting designers, electricians, sound engineers,
		                             branding experts, and interior designers.</p></div>
	</div>
	
	<!-- get in touch -->
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/bg_services in home.jpg') no-repeat;background-size:cover; height: 500px;">
		<div class="hero-img-quote">
			<p>"We strongly believe that good ideas and dynamic presentation do not necessarily lead to a large expense.
			 We always look to deliver "value for money" events, maximizing the budget available to deliver the best
			 possible presentation."</p>
			 <p class="quote-author">Ann Cunano, Commercial Bank Qatar</p>
		</div>
	</div>

	<div class="event-help">
		<h3>Need help with your event?</h3>
		<a class="button" href="contact-us">GET IN TOUCH &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a></p>
	</div>

</div>

<?php get_footer(); ?> 