<?php get_header(); ?>
<link rel="icon" type="image/png" href="<?php bloginfo('template_url');?>/images/logo.png">

<div class="main-content">
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/hero_home.jpg') no-repeat; background-size:cover;">
		<div class="cover-label-home">
			<h1 class="label-title">Creating Experience</h1>
			<h3 class="label-desc">Whatever your event is, we got it covered.</h3>
			<a class="button" href="experience">LEARN MORE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
			<div style="height:90px;"></div>
			<div class="clear"></div>
			<div class="clear"></div>
			<div class="clear"></div>
			<div class="clear"></div>
			<div class="clear"></div>
			<p class="hero-img-label">Qatar International Food Festival<br>
				Museum of Islamic Art Park</p>
		</div>
	</div>

	<div class="content-desc">
		<h4 class="slogan">FROM PLANNING TO EXECUTION</h4>
		<h2 class="description">Events that make an impression</h2>
		<h3 class="information">Impact events was honed from over 30 years of international events expertise.
								We offer complete events solution, from planning to execution and analyis,
								that creates expereinces out of the ordinary.</h3>
	</div>
	<div class="clear"></div>

	<!--Services Slider-->
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/bg_services in home.jpg') no-repeat;background-size:cover;position:absolute;height:650px;"></div>
		<div class="slider3">
			<div class="service-slider3">
				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Events Management and Consultancy</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#events-management-and-consultancy">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				 <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Logistical Support</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qnd_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We offer a complete range of logistical support for your event - equipment, materials
					  		   , crew - all at a fair price.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#logistical-support">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Exhibition Stands</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We build custom exhibition stands in the highest professional quality.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#exhibition-stands">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Stage, Set, Fit-out, Prop, Seating Construction </h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#stage_set_fit-out_prop_seating-construction">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Exhibitions, Openings, Product Launches</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#exhibitions_openings_product-launches">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>			 
					 </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Event Workforce Support</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#event-workforce-support">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>
			</div>
		</div>

		<!-- 2 Slides Slider -->
		<div class="slider2">
			<div class="service-slider2">
				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Events Management and Consultancy</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#events-management-and-consultancy">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>
				 <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Logistical Support</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qnd_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We offer a complete range of logistical support for your event - equipment, materials
					  		   , crew - all at a fair price.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#logistical-support">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Exhibition Stands</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We build custom exhibition stands in the highest professional quality.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#exhibition-stands">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Stage, Set, Fit-out, Prop, Seating Construction </h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#stage_set_fit-out_prop_seating-construction">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Exhibitions, Openings, Product Launches</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#exhibitions_openings_product-launches">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>			 
					 </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Event Workforce Support</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#event-workforce-support">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>
			</div>
		</div>

		<!-- 1 Slide Slider -->
		<div class="slider1">
			<div class="service-slider1">
				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Events Management and Consultancy</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#events-management-and-consultancy">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>
				 <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Logistical Support</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qnd_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We offer a complete range of logistical support for your event - equipment, materials
					  		   , crew - all at a fair price.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#logistical-support">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Exhibition Stands</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We build custom exhibition stands in the highest professional quality.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#exhibition-stands">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Stage, Set, Fit-out, Prop, Seating Construction </h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#stage_set_fit-out_prop_seating-construction">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Exhibitions, Openings, Product Launches</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#exhibitions_openings_product-launches">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>			 
					 </div>

				  <div class="slide">
					  	<div class="slide-title">
					  		<h3 class="services">SERVICES</h3>
					  		<hr style="border-color:#fff;display:block; width:6%; position:absolute; float:left; margin-top:-2px; margin-left: 40px;">
					  		<div class="clear"></div>
					  		<h3 class="services-title">Event Workforce Support</h3>
					  	</div>
					  	<div class="slide-img">
					  		<img src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
					  	</div>
					  	<div class="slide-desc">
					  		<p>We have the experience, contacts, and support services to make your event, opening, exhibition
					  		   or product launch a success.</p>
					  	</div>
					  	<div class="slide-learn">
					  		<a href="services#event-workforce-support">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
					  	</div>
				  </div>
			</div>
		</div>

	<div class="content-desc">
		<h4 class="slogan">HARD AT WORK</h4>
		<h2 class="description">Dynamic, practical, functional</h2>
		<h3 class="information">Impact events has been actively participating in a wide array of events in Doha.
								Take a look in some of our recent work.</h3>
	</div>

	<div class="events-container">
		<div class="events-img">
			<img class="large-img" src="<?php bloginfo('template_url');?>/images/qiff_a.jpg">
			<img class="small-img" src="<?php bloginfo('template_url');?>/images/qiff_b.jpg" style="float:left;">
		</div>
		<div class="events-desc" style="text-align:left;">
			<div class="events-desc-title">
				<h3>QATAR INTERNATIONAL FOOD FESTIVAL 2013-2015</h3>
			</div>
			<div class="events-desc-info">
				<p>For three years running, Impact Events has been contracted by Fischer Alpert to supply
				   logistical support such as carpeting, carpentry and tent fit-out, park and ride service
				   , and cleaning services.</p>

				<p>All of these services comprise of a workload of 300.</p>
			</div>
			<div class="events-desc-learn">
				<a href="experience#Qatar-International-Food-Festival">LEARN MORE 
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
			</div>
		</div>
	</div>

	<div class="events-div1">
		<div class="events-container">
			<div class="events-desc" style="text-align:right; padding-right: 40px;">
				<div class="events-desc-title">
					<h3>QATAR NATIONAL DAY<br>2008-2014</h3>
				</div>
				<div class="events-desc-info">
					<p>Impact Events has been intimately involved with the organization of Qatar National Day since
					   its inception in 2008. We have provided various services from consultancy, to processing fireworks permission,
					   set-building, booth construction, tent dressing and fit-out.</p>

					
				</div>
				<div class="events-desc-learn">
					<a href="experience#Qatar-National-Day">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
				</div>
			</div>
			<div class="events-img">
				<img src="<?php bloginfo('template_url');?>/images/qnd_b.jpg" style="float:right; margin-bottom:-70px;">
				<img src="<?php bloginfo('template_url');?>/images/qnd_a.jpg">
			</div>
		</div>
	</div>

	<div class="events-div2">
		<div class="events-container">
			<div class="events-img">
				<img class="small-img" src="<?php bloginfo('template_url');?>/images/qnd_b.jpg" style="float:right; margin-bottom:-70px;">
				<img src="<?php bloginfo('template_url');?>/images/qnd_a.jpg">
			</div>
			<div class="events-desc" style="text-align:left;">
				<div class="events-desc-title">
					<h3>QATAR NATIONAL DAY<br>2008-2014</h3>
				</div>
				<div class="events-desc-info">
					<p>Impact Events has been intimately involved with the organization of Qatar National Day since
					   its inception in 2008. We have provided various services from consultancy, to processing fireworks permission,
					   set-building, booth construction, tent dressing and fit-out.</p>

					
				</div>
				<div class="events-desc-learn">
					<a href="experience#Qatar-National-Day">LEARN MORE 
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
				</div>
			</div>
		</div>
	</div>

	<div class="events-container">
		<div class="events-img">
			<img class="large-img" src="<?php bloginfo('template_url');?>/images/cbq_a.jpg">
			<img class="small-img" src="<?php bloginfo('template_url');?>/images/cbq_b.jpg" style="float:left;">
		</div>
		<div class="events-desc" style="text-align:left;">
			<div class="events-desc-title">
				<h3>COMMERCIAL BANK QATAR 40TH ANNIVERSARY CELEBRATION</h3>
			</div>
			<div class="events-desc-info">
				<p>We have worked with Fischer Alpert for the three-part celebration of the 40th Anniversary of Commercial Bank Qatar
				   including Booth Branch Tour, Cala Dinner, and Family Day. We provided booth fabrication, tent fit-outs, park and ride services,
				   stage dressing, platform build, and furniture hire.</p>
			</div>
			<div class="events-desc-learn">
				<a href="experience">LEARN MORE 
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a>
			</div>
		</div>
	</div>

	<div class="check-more">
		<div class="check-more-content">
			<div class="check-more-content-text"><p>Like what you see? Check out more of our work.</p></div>
			<div class="check-more-content-btn"><a class="button" href="experience">EXPERIENCE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a></div>
		</div>
	</div>

	<div class="content-desc">
		<h4 class="slogan">HAND IN HAND</h4>
		<h2 class="description">The experts at events</h2>
		<h3 class="information">We work together with our clients to deliver, not only dynamic events,
								but practical and functional productions that exceeds all expectations.</h3>
	</div>

	<div class="experts-container">
		<div class="experts">
			<div class="experts-img" id="russel">
					<img src="<?php bloginfo('template_url');?>/images/team_russell.jpg">
				<a class="overlay" href="team#Russell-John-Mason"><div id="russel-overlay" class="experts-img-overlay"><p>More about<br>Russel</p></div></a>
			</div>
			<div class="experts-info">
				<h4>Russel Mason</h4>
				<p>Managing Director</p>
			</div>
		</div>
		<div class="experts">
			<div class="experts-img" id="perry">
					<img src="<?php bloginfo('template_url');?>/images/team_perry.jpg">
				<a class="overlay" href="team#Perry-Malcolm-Smith"><div id="perry-overlay" class="experts-img-overlay"><p>More about<br>Perry</p></div></a>
			</div>
			<div class="experts-info">
				<h4>Perry Smith</h4>
				<p>Chief Executive Officer</p>
			</div>
		</div>
		<div class="experts">
			<div class="experts-img" id="ann">
					<img src="<?php bloginfo('template_url');?>/images/team_ann.jpg">
				<a class="overlay" href="team#Ann-Karolyn-Cunano"><div id="ann-overlay" class="experts-img-overlay"><p>More about<br>Ann</p></div></a>
			</div>
			<div class="experts-info">
				<h4>Ann Cunano</h4>
				<p>Project Manager & Quality Control</p>
			</div>
		</div>
		<div class="experts">
			<div class="experts-img" id="paul">
					<img src="<?php bloginfo('template_url');?>/images/team_paul.jpg">
				<a class="overlay" href="team#Paul-Sayer"><div id="paul-overlay" class="experts-img-overlay"><p>More about<br>Paul</p></div></a>
			</div>
			<div class="experts-info">
				<h4>Paul Sayers</h4>
				<p>Project Manager & Master Carpenter</p>
			</div>
		</div>
		<div class="experts">
			<div class="experts-img" id="saroj">
					<img src="<?php bloginfo('template_url');?>/images/team_saroj.jpg">
				<a class="overlay" href="team#Saroj-Choudhary"><div id="saroj-overlay" class="experts-img-overlay"><p>More about<br>Saroj</p></div></a>
			</div>
			<div class="experts-info">
				<h4>Saroj Choudhary</h4>
				<p>Crew Chief</p>
			</div>
		</div>
		<div class="experts">
			<div class="experts-img" id="faisal">
					<img src="<?php bloginfo('template_url');?>/images/team_faisal.jpg">
				<a class="overlay" href="team#Faisal-Paduvingal"><div id="faisal-overlay" class="experts-img-overlay"><p>More about<br>Faisal</p></div></a>
			</div>
			<div class="experts-info">
				<h4>Faisal Paduvingal</h4>
				<p>Production Runner</p>
			</div>
		</div>
	</div>

	<div class="partners-container">
		<div class="partners">
			<h3>WE'RE PROUD TO HAVE WORKED WITH</h3>
			<div class="clear"></div>
			<img src="<?php bloginfo('template_url');?>/images/client_qiff.jpg">
			<img src="<?php bloginfo('template_url');?>/images/client_cbq.jpg">
			<img src="<?php bloginfo('template_url');?>/images/client_doha2012.jpg">
			<img src="<?php bloginfo('template_url');?>/images/client_tartans.jpg">
			<img src="<?php bloginfo('template_url');?>/images/client_dragonfest.jpg">
			<img src="<?php bloginfo('template_url');?>/images/client_qnd.jpg">
			<img src="<?php bloginfo('template_url');?>/images/client_lexus.jpg">
			<img src="<?php bloginfo('template_url');?>/images/client_fifa.jpg">
			<img src="<?php bloginfo('template_url');?>/images/client_icss.jpg">
			<div class="clear"></div>
			<div class="clear"></div>
		</div>
	</div>

<!-- get in touch -->
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/bg_services in home.jpg') no-repeat;background-size:cover; height: 500px; margin-top:-90px !important;">
		<div class="hero-img-quote">
			<p>"We strongly believe that good ideas and dynamic presentation do not necessarily lead to a large expense.
			 We always look to deliver "value for money" events, maximizing the budget available to deliver the best
			 possible presentation."</p>
			 <p class="quote-author">Ann Cunano, Commercial Bank Qatar</p>
		</div>
	</div>

	<div class="event-help">
		<h3>Need help with your event?</h3>
		<a class="button" href="contact-us">GET IN TOUCH &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a></p>
	</div>

</div>

<?php get_footer(); ?> 