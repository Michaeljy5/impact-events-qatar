<?php
/*
 Template Name: experience
*/
?>
<link rel="icon" type="image/png" href="<?php bloginfo('template_url');?>/images/logo.png">
<body id="experience">
<?php get_header(); ?>

<div class="main-content">
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/hero_home.jpg') no-repeat; background-size:cover;">
		<div class="cover-label-home">
			<h1 class="label-title">Dynamic, practical, functional</h1>
			<h3 class="label-desc">We are an active participant in a wide array of events in Doha.</h3>
			<div class="clear"></div>
		</div>
	</div>
	
	<div class="experience-content" id="Qatar-International-Food-Festival">
		<div class="experience-logo"><img src="<?php bloginfo('template_url');?>/images/client_qiff.jpg"></div>
		<div class="experience-title"><h2>Qatar International Food Festival</h2></div>
		<div class="experience-info"><h3>Museum of Islamic Art Park, 2013, 2014, 2015</h3></div>
		<div class="experience-slider">
			<div><img src="<?php bloginfo('template_url');?>/images/qiff_b.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/cbq_b.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/qiff_b.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/cbq_b.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/qiff_b.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/cbq_b.jpg"></div>
		</div>
		<div class="clear"></div>
		<div class="experience-desc">
			<p>For three years running, Impact Events has been contracted by Fischer Appelt to supply logistical support such as:</p>
			<p><strong>CARPENTING -</strong> We supplied and installed red carpets totalling 800sqm for an oudoor welcome ceremony, from the entrance of the museum of Islamic Art Park right through to the stage.</p>
			<p><strong>CARPENTRY AND TEN FIT-OUT -</strong> We provide carpenters to work on the install and de-rig for the whole event, working in two shifts and fitted out several tents according to the client's design.</p>
			<p><strong>PARK AND RIDE SERVICE -</strong> A turnkey solution providing transportation services from the parking lots into the event venue. We provided several buses and managed the whole process of "parking" at the parking lot, and "transporting" the guests to the venue and vice versa.</p>
			<p><strong>CLEANING SERVICES -</strong> We provided around 40 cleaners per day to take care of keeping the venue clean and tidy for the whole event duration. Our services include provision of cleaning materials, as well as skip collection for the rubbish.</p>
		</div>
	</div>

	<div class="experience-content" id="Qatar-National-Day">
		<div class="experience-logo"><img src="<?php bloginfo('template_url');?>/images/client_qnd.jpg"></div>
		<div class="experience-title"><h2>Qatar National Day</h2></div>
		<div class="experience-info"><h3>Corniche/Darb A Saji, Doha Qatar, December 2014</h3></div>
		<div class="experience-slider">
			<div><img src="<?php bloginfo('template_url');?>/images/qnd_a.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/qnd_b.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/qnd_a.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/qnd_a.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/qnd_b.jpg"></div>
			<div><img src="<?php bloginfo('template_url');?>/images/qnd_a.jpg"></div>
		</div>
		<div class="clear"></div>
		<div class="experience-desc">
			<p>Impact Events has, in one way or another, been intimately involved with the organization of Qatar Natinal Day since its start in 2008. In their earlier years,
			   we have provided advisory and consultancy services to the State National Day Celebrations Organizing Committee (SNDCOC) during the inception, production and 
               post-production stages of the annual event with our Managing Partner, Russell Mason, acting as an Events Consultant for SNDCOC. In the later years, we have 
               served as an official contractor / sub-contractor in providing various services that ranges from fireworks event permission processing, set-bulding, provision of labor,
               booth construction to tent dressing and fitting-out.
			</p>
			<p><strong>TENT FIT-OUTS -</strong> In 2014, we have successfully fitted out the Al Maqlat Tent and Doha Tent in Darb A Saii which includes fabricating large booths, creating a live-cooking theatre, creating and dressing up partitions, building stages and backdrops, amongst others.</p>
			<p><strong>FURNITURE HIRE -</strong> We hired out several office furniture (desks, chairs, conference tables, drawers), banquet chairs, dining tables, stanchions, VIP chairs and even traditional majlis pieces.</p>
			<p><strong>CARPENTRY SERVICES -</strong> As a sub-contructor to Marco Guidetti, we have provided several carpenters, working for months to build a Souq-Waqif style street at Darb A Saai.</p>
			<p><strong>FIREWORKS LOGISTICS -</strong> For both Corniche and Darb A Saai sites, we provided all necessary logistical support for the fireworks contractor, Howard & Sons.</p>
		</div>
	</div>

	<!-- get in touch -->
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/bg_services in home.jpg') no-repeat;background-size:cover; height: 500px;">
		<div class="hero-img-quote">
			<p>"We strongly believe that good ideas and dynamic presentation do not necessarily lead to a large expense.
			 We always look to deliver "value for money" events, maximizing the budget available to deliver the best
			 possible presentation."</p>
			 <p class="quote-author">Ann Cunano, Commercial Bank Qatar</p>
		</div>
	</div>

	<div class="event-help">
		<h3>Need help with your event?</h3>
		<a class="button" href="contact-us">GET IN TOUCH &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right fa-lg"></i></a></p>
	</div>
	
</div>

<?php get_footer(); ?> 