<?php
/*
 Template Name: contact-us
*/
?>
<link rel="icon" type="image/png" href="<?php bloginfo('template_url');?>/images/logo.png">
<body id="contact-us">
<?php get_header(); ?>

<script>
 $(document).ready(function(){
  $("#contact-submit").click(function(){
   var name = $("#your-name").val();
   var email = $("#your-email").val();
   var message = $("#your-message").val();
   // Returns successful data submission message when the entered information is stored in database.
   var dataString = 'your-name='+ name + '&your-email='+ email + '&your-message='+ message;

    // AJAX Code To Submit Form.
    $.ajax({
     type: "POST",
     url: "http://localhost/impact-events/wp-content/themes/impact/email_form.php",
     data: dataString,
     cache: false,
     success: function(result){
      //alert(result);  
      $("#wpcf7-response-output").html(result);
     }
    });

   return false;
  });
 });
</script>

<div class="main-content">
	<div class="cover-photo-home" style="background:linear-gradient(rgba(105, 26, 64, 0.7), rgba(105, 26, 64, 0.7)),url('<?php bloginfo('template_url');?>/images/hero_home.jpg') no-repeat; background-size:cover;">
		<div class="cover-label-home">
			<h1 class="label-title">Get in touch</h1>
			<h3 class="label-desc">We work closely with our clients to deliver events that exceed all expectations.</h3>
			<div class="clear"></div>
		</div>
	</div>

	<div class="form-contact">
		<li id="text-3" class="widget widget_text">   
            <div class="textwidget"><div role="form" >
                    <div class="screen-reader-response"></div>
                    <form name="" action="" method="post" class="wpcf7-form">
                        <p>Name*<br><span class="wpcf7-form-control-wrap your-name"><input type="text" id="your-name" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name" /></span></p>
                        <p>Email*<br><span class="wpcf7-form-control-wrap your-email"><input type="email" id="your-email"  value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email Address" /></span></p>
                        <p>Message*<br><span class="wpcf7-form-control-wrap your-message"><textarea id="your-message" name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message"></textarea></span></p>
                        <p><input id="contact-submit" type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit" /></p>
                        <div id="wpcf7-response-output" class="contact_message" style="font-family: 'Lato-Light';"></div>
                    </form>
                </div>
            </div>
        </li>  
	</div>

	<div class="contact-content">
     <div class="contact-map2">
       <img src="<?php bloginfo('template_url');?>/images/impact-map.jpg">
    </div>
			<div class="contact-desc">
  				<h3>IMPACT EVENTS QATAR</h3>
  				<p>14th Floor, Commercial Bank Tower, West Bay<br>
  				   P.O.BOX 27111<br> 
  				   Doha, State of Qatar.</p>
  				 <p>Office:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +974 4452 8987<br>
  				   Mobile:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +974 5597 9832<br>
  				   Fax:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +974 4452 8383
  				</p>
			</div>
  		<div class="contact-map">
  			 <img src="<?php bloginfo('template_url');?>/images/impact-map.jpg">
  		</div>
  </div>
</div>
    
<?php get_footer(); ?> 