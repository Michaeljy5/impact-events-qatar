

<footer>
	<div class="footer-content">
		<div class="footer-credits">
			<div class="footer-address">
				<p>14th Floor, Commercial Bank Tower, West Bay<br>
				   P.O.BOX 27111 Doha, State of Qatar.</p>
			</div>
			<div class="footer-contact">
				<p>Office:&nbsp;&nbsp;&nbsp; +974 4452 8987<br>
				   Mobile:&nbsp; +974 5597 9832<br>
				   Fax:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +974 4452 8383<br>
				</p>
			</div>
			<div class="footer-mail">
				<p>info@impacteventsqatar.com</p>
			</div>
		</div>
	</div>
</footer>
</div>
</body>
</html>
<?php wp_footer(); ?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="<?php bloginfo('template_url');?>/bxslider/jquery.bxslider.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.service-slider3').bxSlider({
		controls: false,
		slideWidth: 300,
		minSlides: 3,
		maxSlides: 3,
		moveSlides: 3,
		slideMargin: 30
	});
});
$(document).ready(function(){
	$('.service-slider2').bxSlider({
		controls: false,
		slideWidth: 300,
		minSlides: 2,
		maxSlides: 2,
		moveSlides: 2,
		slideMargin: 30
	});
});
$(document).ready(function(){
	$('.service-slider1').bxSlider({
		controls: false,
		slideWidth: 300,
		minSlides: 1,
		maxSlides: 1,
		moveSlides: 1,
		slideMargin: 5
	});
});

$(document).ready(function(){
	$('.experience-slider').bxSlider({
		controls: true,
		pager: false,
		minSlides: 2,
		maxSlides: 3,
		slideWidth: 290,
		slideMargin: 1,
		nextText: '<img src="<?php bloginfo("template_url");?>/bxslider/images/next-btn.png" height="30" width="20"/>',
		prevText: '<img src="<?php bloginfo("template_url");?>/bxslider/images/prev-btn.png" height="30" width="20"/>'
	});
});

$(".toggle-btn").click(function () {
    $('.toggle-nav').css("margin-right", "0");
});

$(".back-btn").click(function () {
    $('.toggle-nav').css("margin-right", "-420");
});

 $("#russel").hover(
	  function () {
	    $("#russel-overlay").addClass('overlay-hover');
	  },
	  function () {
	    $("#russel-overlay").removeClass("overlay-hover");
	  }
);
$("#perry").hover(
	  function () {
	    $("#perry-overlay").addClass('overlay-hover');
	  },
	  function () {
	    $("#perry-overlay").removeClass("overlay-hover");
	  }
);
$("#ann").hover(
	  function () {
	    $("#ann-overlay").addClass('overlay-hover');
	  },
	  function () {
	    $("#ann-overlay").removeClass("overlay-hover");
	  }
);
$("#paul").hover(
	  function () {
	    $("#paul-overlay").addClass('overlay-hover');
	  },
	  function () {
	    $("#paul-overlay").removeClass("overlay-hover");
	  }
);
$("#saroj").hover(
	  function () {
	    $("#saroj-overlay").addClass('overlay-hover');
	  },
	  function () {
	    $("#saroj-overlay").removeClass("overlay-hover");
	  }
);
$("#faisal").hover(
	  function () {
	    $("#faisal-overlay").addClass('overlay-hover');
	  },
	  function () {
	    $("#faisal-overlay").removeClass("overlay-hover");
	  }
);

</script>




    </body>
</html>
